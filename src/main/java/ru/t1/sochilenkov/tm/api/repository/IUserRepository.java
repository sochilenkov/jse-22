package ru.t1.sochilenkov.tm.api.repository;

import ru.t1.sochilenkov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User findByLogin(String login);

    User findByEmail(String email);

    Boolean isLoginExist(String login);

    Boolean isEmailExist(String email);

}
