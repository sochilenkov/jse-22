package ru.t1.sochilenkov.tm.service;

import ru.t1.sochilenkov.tm.api.service.IProjectService;
import ru.t1.sochilenkov.tm.enumerated.Status;
import ru.t1.sochilenkov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.sochilenkov.tm.exception.field.IdEmptyException;
import ru.t1.sochilenkov.tm.exception.field.IndexIncorrectException;
import ru.t1.sochilenkov.tm.exception.field.NameEmptyException;
import ru.t1.sochilenkov.tm.exception.field.UserIdEmptyException;
import ru.t1.sochilenkov.tm.model.Project;
import ru.t1.sochilenkov.tm.api.repository.IProjectRepository;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(final IProjectRepository repository) {
        super(repository);
    }

    @Override
    public Project changeProjectStatusById(final String userId, final String id, final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Project project = repository.findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final String userId, final Integer index, final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        final Project project = repository.findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project create(final String userId, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = new Project();
        project.setName(name);
        if (description != null && !description.isEmpty())
            project.setDescription(description);
        return add(userId, project);
    }

    @Override
    public Project updateById(final String userId, final String id, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = repository.findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final String userId, final Integer index, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = repository.findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

}
