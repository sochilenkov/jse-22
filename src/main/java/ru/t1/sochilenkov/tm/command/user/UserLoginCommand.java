package ru.t1.sochilenkov.tm.command.user;

import ru.t1.sochilenkov.tm.api.service.IAuthService;
import ru.t1.sochilenkov.tm.enumerated.Role;
import ru.t1.sochilenkov.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    public static final String DESCRIPTION = "Login in application.";

    public static final String NAME = "login";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        final IAuthService authService = getAuthService();
        authService.login(login, password);
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
