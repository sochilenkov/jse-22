package ru.t1.sochilenkov.tm.command.user;

import ru.t1.sochilenkov.tm.enumerated.Role;
import ru.t1.sochilenkov.tm.util.TerminalUtil;

public final class UserRemoveCommand extends AbstractUserCommand {

    public static final String DESCRIPTION = "Remove user from the application.";

    public static final String NAME = "user-remove";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER REMOVE]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        getUserService().removeByLogin(login);
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
